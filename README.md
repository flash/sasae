# Sasae

This library has been deprecated and its functionality is now implemented into [Index](https://patchii.net/flash/index).

## Original header

Sasae is a simple wrapper around some of Twig's functionality as well as making `Twig\Environment` a little bit more immutable.

While it's not a lot of extras, I often implement the added functionality across projects.
The source file structure is meant to be similar to Twig's own.


## Requirements and Dependencies

Sasae currently targets **PHP 8.3**.

No additional requirements and/or dependencies at this time.


## Versioning

Sasae versioning will follows the [Semantic Versioning specification v2.0.0](https://semver.org/spec/v2.0.0.html).

Changes to minimum required PHP version, major Twig library releases that cause incompatibilities and other major overhauls to Sasae itself that break compatibility will be reasons for incrementing the major version.
Updates to Sasae functionality or adjustments to fit new Twig functionality will cause increment the minor version.
Bug fixes and inconsequential Twig library updates will increment the patch version.

Sasae also depends on Index, but its versioning depends on the minimum PHP version and should thus be fairly inconsequential.

Previous major versions may be supported for a time with backports depending on what projects of mine still target older versions of PHP.

The version is stored in the root of the repository in a file called `VERSION` and can be read out within Sasae using `Sasae\SasaeEnvironment::getSasaeVersion()`.


## Contribution

By submitting code for inclusion in the main Sasae source tree you agree to transfer ownership of the code to the project owner.
The contributor will still be attributed for the contributed code, unless they ask for this attribution to be removed.
This is to avoid intellectual property rights traps and drama that could lead to blackmail situations.
If you do not agree with these terms, you are free to fork off.


## Licencing

Sasae is available under the BSD 3-Clause Clear License, a full version of which is enclosed in the LICENCE file.
