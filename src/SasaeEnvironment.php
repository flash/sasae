<?php
// SasaeEnvironment.php
// Created: 2023-08-24
// Updated: 2024-08-04

namespace Sasae;

use InvalidArgumentException;
use UnexpectedValueException;
use Sasae\Cache\SasaeFilesystemCache;
use Sasae\Extension\SasaeExtension;
use Sasae\Loader\SasaeFilesystemLoader;
use Twig\{TwigFilter,TwigFunction,TwigTest};
use Twig\Environment as TwigEnvironment;
use Twig\Cache\CacheInterface as TwigCacheInterface;
use Twig\Extension\ExtensionInterface as TwigExtensionInterface;
use Twig\Extra\Html\HtmlExtension as TwigHtmlExtension;
use Twig\Loader\LoaderInterface as TwigLoaderInterface;

/**
 * Provides a wrapper of Twig\Environment.
 */
class SasaeEnvironment {
    private TwigEnvironment $env;

    /**
     * @param TwigLoaderInterface|string $loader A template loader instance or a path.
     * @param TwigCacheInterface|array<string>|string|null $cache A caching driver.
     * @param string $charset Character for templates.
     * @param bool $debug Debug mode.
     */
    public function __construct(
        TwigLoaderInterface|string $loader,
        TwigCacheInterface|array|string|null $cache = null,
        string $charset = 'utf-8',
        bool $debug = false
    ) {
        if(is_string($loader))
            $loader = new SasaeFilesystemLoader($loader);

        if(is_array($cache)) {
            if(empty($cache))
                throw new InvalidArgumentException('If $cache is an array, it may not be empty.');
            $cache = SasaeFilesystemCache::create(array_shift($cache), array_shift($cache));
        } elseif($cache === null) $cache = false;

        $this->env = new TwigEnvironment($loader, [
            'debug' => $debug,
            'cache' => $cache,
            'charset' => $charset,
            'strict_variables' => true, // there's no reason to disable this ever
        ]);

        $this->env->addExtension(new TwigHtmlExtension);
        $this->env->addExtension(new SasaeExtension);
    }

    /**
     * Get a reference to the underlying Twig Environment.
     * Things that aren't exposed through Sasae generally have a reason
     *  for being "obfuscated" but go wild if you really want to.
     *
     * @return TwigEnvironment
     */
    public function getEnvironment(): TwigEnvironment {
        return $this->env;
    }

    /**
     * Returns if debug mode is enabled.
     *
     * @return bool
     */
    public function isDebug(): bool {
        return $this->env->isDebug();
    }

    /**
     * Registers an extension.
     *
     * @param TwigExtensionInterface $extension
     */
    public function addExtension(TwigExtensionInterface $extension): void {
        $this->env->addExtension($extension);
    }

    /**
     * Registers a filter.
     *
     * @param string $name Name of the filter.
     * @param callable $body Body of the filter.
     * @param array<string, mixed> $options Options, review the TwigFilter file for the options.
     */
    public function addFilter(string $name, callable $body, array $options = []): void {
        $this->env->addFilter(new TwigFilter($name, $body, $options));
    }

    /**
     * Registers a function.
     *
     * @param string $name Name of the function.
     * @param callable $body Body of the function.
     * @param array<string, mixed> $options Options, review the TwigFunction file for the options.
     */
    public function addFunction(string $name, callable $body, array $options = []): void {
        $this->env->addFunction(new TwigFunction($name, $body, $options));
    }

    /**
     * Registers a twig.
     *
     * @param string $name Name of the twig.
     * @param callable $body Body of the twig.
     * @param array<string, mixed> $options Options, review the TwigTest file for the options.
     */
    public function addTest(string $name, callable $body, array $options = []): void {
        $this->env->addTest(new TwigTest($name, $body, $options));
    }

    /**
     * Adds a global variable available in any SasaeContext instance.
     *
     * @param string $name Name of the variable.
     * @param mixed $value Content of the variable.
     */
    public function addGlobal(string $name, mixed $value): void {
        $this->env->addGlobal($name, $value);
    }

    /**
     * Loads a template and creates a SasaeContext instance.
     *
     * @param string $name Name or path of the template.
     * @param array<string, mixed> $vars Context local variables to add right away.
     * @return SasaeContext
     */
    public function load(string $name, array $vars = []): SasaeContext {
        return new SasaeContext($this->env->load($name), $vars);
    }

    /**
     * Direct proxy to TwigEnvironment's render method.
     *
     * @param string $name Name or path of the template.
     * @param array<string, mixed> $vars Local variables to render the template with.
     * @return string
     */
    public function render(string $name, array $vars = []): string {
        return $this->env->render($name, $vars);
    }
}
