<?php
// SasaeExtension.php
// Created: 2024-08-04
// Updated: 2024-08-04

namespace Sasae\Extension;

use Index\{ByteFormat,Index};
use Sasae\{Sasae,SasaeEnvironment};
use Twig\{TwigFilter,TwigFunction};
use Twig\Environment as TwigEnvironment;
use Twig\Extension\AbstractExtension as TwigAbstractExtension;

/**
 * Provides version functions and additional functionality implemented in Index.
 */
class SasaeExtension extends TwigAbstractExtension {
    public function getFilters() {
        return [
            new TwigFilter('format_filesize', ByteFormat::format(...)),
        ];
    }

    public function getFunctions() {
        return [
            new TwigFunction('ndx_version', Index::version(...)),
            new TwigFunction('sasae_version', Sasae::version(...)),
            new TwigFunction('twig_version', fn() => TwigEnvironment::VERSION),
        ];
    }
}
