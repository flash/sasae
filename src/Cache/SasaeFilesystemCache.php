<?php
// SasaeFilesystemCache.php
// Created: 2024-08-04
// Updated: 2024-08-04

namespace Sasae\Cache;

use Twig\Cache\FilesystemCache as TwigFilesystemCache;
/**
 * Extends Twig's filesystem cache implementation with an alternate constructor.
 */
class SasaeFilesystemCache extends TwigFilesystemCache {
    /**
     * @param string $path Directory path to store the cache in.
     * @param bool $autoReload Whether to refresh the cache if changes are detected.
     */
    public function __construct(string $path, bool $autoReload) {
        parent::__construct(
            $path,
            $autoReload ? TwigFilesystemCache::FORCE_BYTECODE_INVALIDATION : 0
        );
    }

    /**
     * Creates an instance of the filesystem cacher in the system temporary path based on project name and version.
     *
     * @param string $name Name of the project in a format the filesystem will be happy with.
     * @param ?string $version Version of the project in a format the filesystem will be happy with or null to enable auto reload.
     */
    public static function create(string $name, ?string $version): self {
        $autoReload = $version === null;
        $path = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'sasae-' . $name;
        if(!$autoReload)
            $path .= '-' . $version;

        return new self($path, $autoReload);
    }
}
