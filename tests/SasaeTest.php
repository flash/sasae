<?php
// SasaeTest.php
// Created: 2024-08-04
// Updated: 2024-08-04

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use Index\{Index,XString};
use Sasae\{Sasae,SasaeContext,SasaeEnvironment};
use Sasae\Cache\SasaeFilesystemCache;
use Sasae\Extension\SasaeExtension;
use Sasae\Loader\SasaeFilesystemLoader;
use Twig\Environment as TwigEnvironment;

#[CoversClass(Sasae::class)]
#[CoversClass(SasaeContext::class)]
#[CoversClass(SasaeEnvironment::class)]
#[CoversClass(SasaeFilesystemCache::class)]
#[CoversClass(SasaeExtension::class)]
#[CoversClass(SasaeFilesystemLoader::class)]
final class SasaeTest extends TestCase {
    public function testEverything(): void {
        $env = new SasaeEnvironment(
            __DIR__,
            ['SasaeTest', XString::random(8)]
        );

        $this->assertFalse($env->isDebug());

        $env->addGlobal('global_var', 'Sasae global var');
        $env->addGlobal('expect', [
            'ndx_version' => Index::version(),
            'sasae_version' => Sasae::version(),
            'twig_version' => TwigEnvironment::VERSION,
        ]);

        $env->addFilter('test_filter', fn($text) => ('filter:' . $text));
        $env->addFunction('test_function', fn($text) => ('func:' . $text));
        $env->addTest('test_test', fn($text) => $text === 'test');

        $rendered = $env->render('test-rendered', [
            'local_var' => 'this var is local',
        ]);

        $this->assertEquals(file_get_contents(__DIR__ . '/test-rendered.html'), $rendered);

        $ctx = $env->load('test-loaded', [
            'context_var' => 'this var is context',
            'variant' => 'toString()',
        ]);

        $ctx->setVar('simple_set', 'simple set call');
        $ctx->setVar('another.context.var.deep', 'applied with fuckery');

        $ctx->setVars([
            'context_var2' => 'applied without fuckery',
        ]);

        $loaded = $ctx->render([
            'local_var' => 'this var is local',
            'variant' => 'render()',
        ]);

        $this->assertEquals(file_get_contents(__DIR__ . '/test-loaded-render.html'), $loaded);
        $this->assertEquals(file_get_contents(__DIR__ . '/test-loaded-string.html'), (string)$ctx);
    }
}
